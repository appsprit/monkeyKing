
set(MODULE_NAME king_http_server)


aux_source_directory(. SOURCES)

add_executable(${MODULE_NAME} ${SOURCES})

target_link_libraries(${MODULE_NAME} king_foundation)
