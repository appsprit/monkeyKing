#define LOG_TAG "http_server"

#include <stdio.h>
#include <foundation/Log.h>

using namespace monkeyking;

int main(int argc, char *argv[])
{
    ALOGV("hello world! %d", 123);

    return 0;
}
