/* 
 * Copyright (C) 2020 Amlogic, Inc. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#define LOG_TAG "Log"
#include "Log.h"
#include <stdarg.h>
#include <stdio.h>
#include <time.h>
#include <sys/syscall.h>
#include <unistd.h>

namespace monkeyking {

//TODO: add a log process backend


int __mking_log_print(int prio, const char* tag, const char* fmt, ...)
{
    char buffer[1024];
    int len = 0;

    struct timespec tp;
    clock_gettime(CLOCK_REALTIME, &tp);

    struct tm t;
    
    localtime_r(&tp.tv_sec, &t);

    len += strftime(buffer+len, sizeof(buffer)-len, "%m-%d %H:%M:%S.", &t);
    len += snprintf(buffer+len, sizeof(buffer)-len, "%.3ld", tp.tv_nsec/1000000);

    int pid = getpid();
    int tid = syscall(__NR_gettid);

    len += snprintf(buffer+len, sizeof(buffer)-len, " %d %d %s ", pid, tid, tag);

    va_list ap;
    va_start(ap, fmt);
    len += vsnprintf(buffer+len, sizeof(buffer)-len, fmt, ap);
    va_end(ap);

    printf("%s\n", buffer);

    return len;
}


}
