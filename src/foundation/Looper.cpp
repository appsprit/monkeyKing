/* 
 * Copyright (C) 2020 Amlogic, Inc. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#define LOG_TAG "Looper"
#include "Looper.h"
#include "Log.h"
#include <sys/eventfd.h>
#include <unistd.h>
#include <string.h>
#include <algorithm>

namespace monkeyking {

static const int EPOLL_MAX_EVENTS = 16;

static pthread_once_t gTLSOnce = PTHREAD_ONCE_INIT;
static pthread_key_t gTLSKey = 0;

int64_t systemTime(int clock)
{
    struct timespec t;
    t.tv_sec = t.tv_nsec = 0;
    clock_gettime(clock, &t);
    return int64_t(t.tv_sec * 1000000LL) + t.tv_nsec/1000;
}

int toMillisecondTimeoutDelay(int64_t referenceTime, int64_t timeoutTime)
{
    int64_t timeoutDelayMillis;

    if (timeoutTime > referenceTime) {
        uint64_t timeoutDelay = uint64_t(timeoutTime - referenceTime);
        if (timeoutDelay > uint64_t(INT32_MAX - 1) * 1000000LL) {
            timeoutDelayMillis = -1;
        } else {
            timeoutDelayMillis = (timeoutDelay + 999LL) / 1000LL;
        }
    } else {
        timeoutDelayMillis = 0;
    }

    return (int)timeoutDelayMillis;
}

Looper::Looper(bool allowNonCallbacks)
: mAllowNonCallbacks(allowNonCallbacks)
, mSendingMessage(false)
, mPolling(false)
, mEpollRebuildRequired(false)
, mNextRequestSeq(0)
, mResponseIndex(0)
, mNextMessageUptime(INT64_MAX)
{
    mWakeEventFd = eventfd(0, EFD_NONBLOCK | EFD_CLOEXEC); 
    if (mWakeEventFd < 0) {
        ALOGE("Could not make wake event fd: %s", strerror(errno));
    }

    std::lock_guard<std::mutex> _l(mLock);
    rebuildEpollLocked();
}

Looper::~Looper()
{

}

void Looper::initTLSKey()
{
    int error = pthread_key_create(&gTLSKey, threadDestructor);
    if (error != 0) {
        ALOGE("Could not allocate TLS key:%s", strerror(errno));
    }
}

void Looper::threadDestructor(void* st)
{

}

void Looper::setForThread(const std::shared_ptr<Looper>& looper)
{
    std::shared_ptr<Looper> old = getForThread();

    pthread_setspecific(gTLSKey, looper.get());
}

std::shared_ptr<Looper> Looper::getForThread()
{
    int result = pthread_once(&gTLSOnce, initTLSKey);

    Looper* looper = (Looper*)pthread_getspecific(gTLSKey);

    return looper->shared_from_this();
}

std::shared_ptr<Looper> Looper::prepare(bool allowNonCallbacks)
{
    std::shared_ptr<Looper> looper = Looper::getForThread();
    if (looper == nullptr) {
        looper.reset(new Looper(allowNonCallbacks));
        Looper::setForThread(looper);
    }

    return looper;
}

bool Looper::getAllowNonCallbacks() const
{
    return mAllowNonCallbacks;
}

void Looper::rebuildEpollLocked()
{
    if (mEpollFd >= 0) {
        ::close(mEpollFd);
        mEpollFd = -1;
    }

    mEpollFd = epoll_create(EPOLL_CLOEXEC);
    if (mEpollFd < 0) {
        ALOGE("Coult not create epoll instance: %s", strerror(errno));
        return;
    }

    struct epoll_event eventItem;
    memset(&eventItem, 0, sizeof(eventItem));
    eventItem.events = EPOLLIN;
    eventItem.data.fd = mWakeEventFd;
    int result = epoll_ctl(mEpollFd, EPOLL_CTL_ADD, mWakeEventFd, &eventItem);
    if (result < 0) {
        ALOGE("Could not add wake event fd to epoll instance:%s", strerror(errno));
    }

    for (size_t i = 0; i < mRequests.size(); ++i) {
        const Request& request = mRequests.at(i);
        struct epoll_event eventItem;
        request.initEventItem(&eventItem);

        int epollResult = epoll_ctl(mEpollFd, EPOLL_CTL_ADD, request.fd, &eventItem);
        if (epollResult < 0) {
            ALOGE("Error adding epoll events for fd %d while rebuiling epoll set: %s", request.fd, strerror(errno));
        }
    }
}

void Looper::scheduleEpollRebuildLocked()
{
    if (!mEpollRebuildRequired) {
        mEpollRebuildRequired = true;
        wake();
    }
}

int Looper::pollOnce(int timeoutMillis, int* outFd, int* outEvents, void** outData)
{
    int result = 0;

    for (;;) {
        while (mResponseIndex < mResponses.size()) {
            const Response& response = mResponses.at(mResponseIndex++);
            int ident = response.request.ident;
            if (ident > 0) {
                int fd = response.request.fd;
                int events = response.events;
                void* data = response.request.data;

                if (outFd != nullptr) *outFd = fd;
                if (outEvents != nullptr) *outEvents = events;
                if (outData != nullptr) *outData = data;
                return ident;
            }
        }

        if (result != 0) {
            if (outFd != nullptr) *outFd = 0;
            if (outEvents != nullptr) *outEvents = 0;
            if (outData != nullptr) *outData = 0;
            return result;
        }

        result = pollInner(timeoutMillis);
    }
}

int Looper::pollInner(int timeoutMillis)
{
    if (timeoutMillis != 0 && mNextMessageUptime != INT64_MAX) {
        int64_t now = systemTime(CLOCK_MONOTONIC);
        int messageTimeoutMillis = toMillisecondTimeoutDelay(now, mNextMessageUptime);
        if (messageTimeoutMillis >= 0 && (timeoutMillis < 0 || messageTimeoutMillis < timeoutMillis)) {
            timeoutMillis = messageTimeoutMillis;
        }
    }

    int result = POLL_WAKE;
    mResponses.clear();
    mResponseIndex = 0;

    mPolling = true;

    struct epoll_event eventItems[EPOLL_MAX_EVENTS];
    int eventCount = epoll_wait(mEpollFd, eventItems, EPOLL_MAX_EVENTS, timeoutMillis);

    mPolling = false;

    mLock.lock();

    if (mEpollRebuildRequired) {
        mEpollRebuildRequired = false;
        rebuildEpollLocked();
        goto done;
    }

    if (eventCount < 0) {
        if (errno == EINTR) {
            goto done;
        }

        ALOGW("Poll failed with an unexpected error: %s", strerror(errno));
        result = POLL_ERROR;
        goto done;
    }

    if (eventCount == 0) {
        result = POLL_TIMEOUT;
        goto done;
    }

    for (int i = 0; i < eventCount; ++i) {
        int fd = eventItems[i].data.fd;
        uint32_t epollEvents = eventItems[i].events;
        if (fd == mWakeEventFd) {
            if (epollEvents & EPOLLIN) {
                awoken();
            } else {
                ALOGW("Ignoring unexpected epoll events 0x%x on wake event fd.", epollEvents);
            }
        } else {
            auto it = mRequests.find(fd);
            if (it != mRequests.end()) {
                int events = 0;
                if (epollEvents & EPOLLIN) events |= EVENT_INPUT;
                if (epollEvents & EPOLLOUT) events |= EVENT_OUTPUT;
                if (epollEvents & EPOLLERR) events |= EVENT_ERROR;
                if (epollEvents & EPOLLHUP) events |= EVENT_HANGUP;
                pushResponse(events, it->second);
            } else {
                ALOGW("Ignoring unexpected epoll events 0x%x on fd %d that is no longer registered.",
                        epollEvents, fd);
            }
        }
    }

done: ;
    
    // Invoke pending message callbacks.
    mNextMessageUptime = INT64_MAX;
    while (mMessageEnvelopes.size() != 0) {
        int64_t now = systemTime(CLOCK_MONOTONIC);
        const MessageEnvelope& messageEnvelope = mMessageEnvelopes.front();
        if (messageEnvelope.uptime < now) {
            {
                std::function<LooperMessageHandler> handler = messageEnvelope.handler;
                Message message = messageEnvelope.message;
                mMessageEnvelopes.erase(mMessageEnvelopes.begin());
                mSendingMessage = true;

                mLock.unlock();
                handler(message);
            }

            mLock.lock();
            mSendingMessage = false;
            result = POLL_CALLBACK;
        } else {
            mNextMessageUptime = messageEnvelope.uptime;
            break;
        }
    }

    mLock.unlock();

    for (size_t i = 0; i < mResponses.size(); ++i) {
        Response &response = mResponses.at(i);
        if (response.request.ident == POLL_CALLBACK) {
            int fd = response.request.fd;
            int events = response.events;
            void* data = response.request.data;

            int callbackResult = response.request.callback(fd, events, data);
            if (callbackResult == 0) {
                removeFd(fd, response.request.seq);
            }

            response.request.callback = nullptr;
            result = POLL_CALLBACK;
        }
    }

    return result;
}

void Looper::wake()
{
    uint64_t inc = 1;
    ssize_t nWrite = write(mWakeEventFd, &inc, sizeof(inc));
    if (nWrite != sizeof(inc)) {
        ALOGE("Could not write wake signal to fd %d (return %zd): %s", mWakeEventFd, nWrite, strerror(errno));
    }
}

void Looper::awoken()
{
    uint64_t counter;

    read(mWakeEventFd, &counter, sizeof(uint64_t));
}

void Looper::pushResponse(int events, const Request& request)
{
    Response response;

    response.events = events;
    response.request = request;
    mResponses.push_back(response);
}

int Looper::addFd(int fd, int ident, int events, const std::function<LooperCallbackFunc>& callback, void* data)
{
    if (callback == nullptr) {
        if (!mAllowNonCallbacks) {
            ALOGE("Invalid attempt to set NULL callback but not allowed for this looper.");
            return -1;
        }

        if (ident < 0) {
            ALOGE("Invalid attempt to set NULL callback with ident < 0");
            return -1;
        }
    } else {
        ident = POLL_CALLBACK;
    }

    {
        std::lock_guard<std::mutex> _l(mLock);

        Request request;
        request.fd = fd;
        request.ident = ident;
        request.events = events;
        request.seq = mNextRequestSeq++;
        request.callback = callback;
        request.data = data;
        if (mNextRequestSeq == -1) mNextRequestSeq = 0;

        struct epoll_event eventItem;
        request.initEventItem(&eventItem);

        auto it = mRequests.find(fd);
        if (it == mRequests.end()) {
            int epollResult = epoll_ctl(mEpollFd, EPOLL_CTL_ADD, fd, &eventItem);
            if (epollResult < 0) {
                ALOGE("Error adding epoll events for fd %d: %s", fd, strerror(errno));
                return -1;
            }

            mRequests.emplace(fd, request);
        } else {
            int epollResult = epoll_ctl(mEpollFd, EPOLL_CTL_MOD, fd, &eventItem);
            if (epollResult < 0) {
                if (errno == ENOENT) {
                    epollResult = epoll_ctl(mEpollFd, EPOLL_CTL_ADD, fd, &eventItem);
                    if (epollResult < 0) {
                        ALOGE("Error modifying or adding epoll events for fd %d: %s", fd, strerror(errno));
                        return -1;
                    }

                    scheduleEpollRebuildLocked();
                } else {
                     ALOGE("Error modifying epoll events for fd %d: %s", fd, strerror(errno));
                     return -1;
                }
            }

            it->second = request; //replace with new request
        }
    }

    return 1;
}

int Looper::removeFd(int fd)
{
    return removeFd(fd, -1);
}

int Looper::removeFd(int fd, int seq)
{
    {
        std::lock_guard<std::mutex> _l(mLock);
        auto it = mRequests.find(fd);
        if (it == mRequests.end()) {
            return 0;
        }

        if (seq != -1 && it->second.seq != seq) {
            return 0;
        }

        mRequests.erase(it);

        int epollResult = epoll_ctl(mEpollFd, EPOLL_CTL_DEL, fd, nullptr);
        if (epollResult < 0) {
            if (seq != -1 && (errno == EBADF || errno == ENOENT)) {
                scheduleEpollRebuildLocked();
            }
        } else {
            scheduleEpollRebuildLocked();
            return -1;
        }
    }

    return 1;
}

void Looper::sendMessage(const std::function<LooperMessageHandler>& handler, const Message& message)
{
    int64_t now = systemTime(CLOCK_MONOTONIC);
    sendMessageAtTime(now, handler, message);
}

void Looper::sendMessageDelayed(int64_t uptimeDelayUs, const std::function<LooperMessageHandler>& handler, const Message& message)
{
    int64_t now = systemTime(CLOCK_MONOTONIC);
    sendMessageAtTime(now + uptimeDelayUs, handler, message);
}


void Looper::sendMessageAtTime(int64_t uptime, const std::function<LooperMessageHandler>& handler, const Message& message)
{
    bool empty = false;

    {
        std::lock_guard<std::mutex> _l(mLock);

        MessageEnvelope messageEnvelope{uptime, handler, message};

        auto it = std::upper_bound(mMessageEnvelopes.begin(), mMessageEnvelopes.end(), uptime, [](int64_t value, const MessageEnvelope& envelope) {return value < envelope.uptime;});
        mMessageEnvelopes.insert(it, messageEnvelope);

        empty = it == mMessageEnvelopes.end();

        if (mSendingMessage) {
            return;
        }
    }

    if (empty) {
        wake();
    }
}

void Looper::removeMessages(int what)
{
    {
        std::lock_guard<std::mutex> _l(mLock);

        for (auto it = mMessageEnvelopes.begin(); it != mMessageEnvelopes.end();) {
            const MessageEnvelope& messageEnvelope = *it;
            if (messageEnvelope.message.what == what) {
                it = mMessageEnvelopes.erase(it);
            } else {
                ++it;
            }
        }
    }
}

bool Looper::isPolling() const
{
    return mPolling;
}

void Looper::Request::initEventItem(struct epoll_event* eventItem) const
{
    int epollEvents = 0;

    if (events & EVENT_INPUT) epollEvents |= EPOLLIN;
    if (events & EVENT_OUTPUT) epollEvents |= EPOLLOUT;

    memset(eventItem, 0, sizeof(struct epoll_event));
    eventItem->events = epollEvents;
    eventItem->data.fd = fd;
}


}
