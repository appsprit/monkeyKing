/* 
 * Copyright (C) 2020 Amlogic, Inc. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#ifndef _LOG_H_
#define _LOG_H_


namespace monkeyking {

enum {
    LOG_VERBOSE,
    LOG_INFO,
    LOG_WARNING,
    LOG_ERROR,
};


int __mking_log_print(int prio, const char* tag, const char* fmt, ...);

#define ALOGV(fmt, ...) __mking_log_print(LOG_VERBOSE, LOG_TAG, fmt, ##__VA_ARGS__)
#define ALOGI(fmt, ...) __mking_log_print(LOG_INFO, LOG_TAG, fmt, ##__VA_ARGS__)
#define ALOGW(fmt, ...) __mking_log_print(LOG_WARNING, LOG_TAG, fmt, ##__VA_ARGS__)
#define ALOGE(fmt, ...) __mking_log_print(LOG_ERROR, LOG_TAG, fmt, ##__VA_ARGS__)

}






#endif
