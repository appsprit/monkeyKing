/* 
 * Copyright (C) 2020 Amlogic, Inc. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#ifndef _LOOPER_H_
#define _LOOPER_H_

#include <functional>
#include <memory>
#include <mutex>
#include <vector>
#include <map>
#include <sys/epoll.h>

namespace monkeyking {

struct Message {
    explicit Message(int w = 0) : what(w) {}

    int what;
};

using LooperCallbackFunc = int(int fd, int events, void* data);
using LooperMessageHandler = void(const Message&);

class Looper : public std::enable_shared_from_this<Looper>
{
public:
    enum {
        POLL_WAKE = -1,
        POLL_CALLBACK = -2,
        POLL_TIMEOUT = -3,
        POLL_ERROR = -4,
    };

    enum {
        EVENT_INPUT = 1 << 0,
        EVENT_OUTPUT = 1 << 1,
        EVENT_ERROR = 1 <<2,
        EVENT_HANGUP = 1 << 3,
        EVENT_INVALID  = 1 << 4,
    };

    explicit Looper(bool allowNonCallbacks);
    ~Looper();
    bool getAllowNonCallbacks() const;
    int pollOnce(int timeoutMillis, int* outFd, int* outEvents, void** outData);
    inline int pollOnce(int timeoutMillis) {
        return pollOnce(timeoutMillis, nullptr, nullptr, nullptr);
    }

    void wake();

    int addFd(int fd, int ident, int events, const std::function<LooperCallbackFunc>& callback, void* data);
    int removeFd(int fd);

    void sendMessage(const std::function<LooperMessageHandler>& handler, const Message& message);
    void sendMessageDelayed(int64_t uptimeDelayUs, const std::function<LooperMessageHandler>& handler, const Message& message);
    void sendMessageAtTime(int64_t uptimeUs, const std::function<LooperMessageHandler>& handler, const Message& message);
    void removeMessages(int what);
    bool isPolling() const;
    static std::shared_ptr<Looper> prepare(bool allowNonCallbacks);
    static void setForThread(const std::shared_ptr<Looper>& looper);
    static std::shared_ptr<Looper> getForThread();

private:
    struct Request {
        int fd;
        int ident;
        int events;
        int seq;
        std::function<LooperCallbackFunc> callback;
        void* data;
        void initEventItem(struct epoll_event* eventItem) const;
    };

    struct Response {
        int events;
        struct Request request;
    };

    struct MessageEnvelope {
        MessageEnvelope() : uptime(0) {}
        MessageEnvelope(int64_t u,  const std::function<LooperMessageHandler>& h, const Message& m)
        : uptime(u), handler(h), message(m) {}

        int64_t uptime;
        std::function<LooperMessageHandler> handler;
        Message message;
    };

    const bool mAllowNonCallbacks;
    int mWakeEventFd;
    std::mutex mLock;
    std::vector<MessageEnvelope> mMessageEnvelopes;
    bool mSendingMessage;
    volatile bool mPolling;

    int mEpollFd;
    bool mEpollRebuildRequired;

    std::map<int, Request> mRequests;
    int mNextRequestSeq;

    std::vector<Response> mResponses;
    size_t mResponseIndex;
    int64_t mNextMessageUptime;

    int pollInner(int timeoutMillis);
    int removeFd(int fd, int seq);
    void awoken();
    void pushResponse(int events, const Request& request);
    void rebuildEpollLocked();
    void scheduleEpollRebuildLocked();

    static void initTLSKey();
    static void threadDestructor(void* st);
    static void initEpollEvent(struct epoll_event* eventItem);

private:
    Looper(const Looper&) = delete;
    Looper& operator= (const Looper&) = delete;
};

}



#endif
